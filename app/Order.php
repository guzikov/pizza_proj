<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Order
 * @package App
 *
 * @property string name
 * @property string mobile
 * @property string message
 * @property string delivery
 * @property int amount
 */
class Order extends Model
{
    //
}
