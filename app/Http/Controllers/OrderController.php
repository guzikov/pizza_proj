<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrder;
use App\Order;
use Illuminate\Support\Facades\DB;

class OrderController extends Controller
{
    public function store(StoreOrder $request)
    {
        $order = new Order();
        $order->name = $request->name;
        $order->mobile = $request->mobile;
        $order->message = $request->message;
        $order->delivery = $request->delivery;
        $order->amount = $request->amount;
        $order->save();

        foreach ($request->order as $pizza) {
            DB::table('orders_pizzas')->insert(['order_id' => $order->id, 'pizza_id' => $pizza['id']]);
        }

        return 1;
    }
}
