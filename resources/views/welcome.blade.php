<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="/img/favicon.ico" type="image/x-icon">

        <title>Pizza Delivery</title>
        <link rel="stylesheet" href="/css/app.css">
    </head>
    <body>
        <div id="app">
            <div class="container">
                <nav class="navbar" role="navigation" aria-label="main navigation">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="https://bulma.io">
                            <img src="https://bulma.io/images/bulma-logo.png" width="112" height="28">
                        </a>

                        <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div>

                    <div id="navbarBasicExample" class="navbar-menu">
                        <div class="navbar-start">
                            <router-link to="/" class="navbar-item" exact>
                                Home
                            </router-link>

                            <router-link to="/pizza" class="navbar-item">
                                Pizza
                            </router-link>

                            <router-link to="/cart" class="navbar-item">
                                Cart
                            </router-link>

                        </div>
                    </div>
                </nav>
                <main class="container">
                    <router-view></router-view>
                </main>
            </div>


        </div>

        <script src="/js/app.js"></script>
    </body>
</html>
