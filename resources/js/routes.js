import Home from './components/Home';
import Cart from './components/Cart';
import Pizza from './components/Pizza';
import NotFound from './components/NotFound';

export default {
    mode: 'history',
    linkActiveClass: 'is-active',
    routes: [
        {
            path: '*',
            component: NotFound
        },
        {
            path: '/',
            component: Home
        },
        {
            path: '/cart',
            component: Cart
        },
        {
            path: '/pizza',
            component: Pizza
        }
    ]
}