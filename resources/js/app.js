import Vue from 'vue';
import VueRouter from 'vue-router';
import Vuex from 'vuex';
import routes from './routes';

Window.axios = require('axios');

Vue.use(VueRouter);
Vue.use(Vuex);

const store = new Vuex.Store({
    state: {
        count: 0,
        order: [],
        set: new Set(),
    },
    mutations: {
        increment (state, product) {
            state.order.push(product)
        },
        decrement (state, product) {
            for (let i =0; i < state.order.length; i++) {
                if (state.order[i].id === product.id) {
                    state.order.splice(i, 1);
                    break;
                }
            }
        },
        delete (state, product) {
            state.order = state.order.filter(pizza => pizza.id !== product.id);
            let newSet = new Set();
            state.set.forEach(function (pizza) {
                pizza = JSON.parse(pizza);
                if (pizza.id !== product.id) {
                    newSet.add(JSON.stringify(pizza));
                }
            });
            state.set = newSet;
        },
    }
});


let app = new Vue({
    el: '#app',
    store,
    router: new VueRouter(routes)
});
