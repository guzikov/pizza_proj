Test application for interview.

Used tech stack:

- Laravel
- Vue
- Vuex
- Vue Router
- MySql
- Bulma as CSS framework

Setup for development environment:

1) clone from git repo
2) composer install
3) npm i
4) php artisan migrate
5) npm run dev

Data for the pizza products are fetching from the DB, so you need to fill it up manually.
